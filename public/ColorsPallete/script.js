
/**************  Functions  ***************/

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function generateHue(prevHue, incre) {
    var max = 355;
    var nextHue = prevHue + incre;
    if (nextHue > max) {
        nextHue -= max;
    }
    return nextHue;
}

function randomHues(length) {
    var min = 0;
    var max = 355;
    var incre = parseInt((max+1)/length);  

    var hues = [];
    var hue = getRandomInt(min, max);
    hues.push(hue);
    for (let i=1; i<length; i++) {
        hue = generateHue(hue, incre);
        hues.push(hue);
    }
    return hues;
}

function randomSaturations(length) {
    var sats = [];
    var init = 359;
    var decre = parseInt(init/length);
    var sat = init;
    sats.push(sat);
    for (let i=1; i<length; i++) {
        sat -= decre;
        sats.push(sat);
    }
    return sats;
}

function randomBrightness(length) {
    var brgs = [];
    var init = 359;
    var decre = parseInt(init/length);
    var brg = init;
    brgs.push(brg);
    for (let i=1; i<length; i++) {
        brg -= decre;
        brgs.push(brg);
    }
    return brgs;
}

function randomColors(rows, cols) {
    // Generate Random Hues
    var h = randomHues(cols);
    var s = randomSaturations(rows);
    var v = randomBrightness(cols);

    // Convert Colors to RGB
    var colors = [];
    for (let i=0; i<rows; i++) {
        var colorsRow = [];
        for (let j=0; j<cols; j++) {
            var color = hsvToRgb(h[j]/359, s[i]/359, v[j]/359);
            colorsRow.push(color);
        }
        colors.push(colorsRow);
    }

    return colors;
}

function generateRules(colors) {
    // Build Rules
    var cssRules = "";
    for (let i=0; i<colors.length; i++) {
        cssRules += "############ CSS "+(i+1)+" ############\n";
        cssRules += "\n.website-background {\n  color: "+rgbToHex(colors[i][0][0], colors[i][0][1], colors[i][0][2])+"; \n}" +
        "\n.element-text {\n  color: "+rgbToHex(colors[i][1][0], colors[i][1][1], colors[i][1][2])+"; \n} " +
        "\n.element-border {\n  border-color: "+rgbToHex(colors[i][2][0], colors[i][2][1], colors[i][2][2])+"; \n}" +
        "\n.element-background {\n  background-color: "+rgbToHex(colors[i][3][0], colors[i][3][1], colors[i][3][2])+"; \n}" +
        "\n.header {\n  color: "+rgbToHex(colors[i][4][0], colors[i][4][1], colors[i][4][2])+"; \n}\n\n";
    }

    return cssRules;
}

function newPaletteRules(colors, textColor) {
    // Generate Palette
    for (let i=0; i<colors.length; i++) {
        // Background-Color
        document.getElementById('div'+(i+1)+'Color1').style.backgroundColor = "rgb("+colors[i][0][0]+", "+colors[i][0][1]+", "+colors[i][0][2]+")";    
        document.getElementById('div'+(i+1)+'Color2').style.backgroundColor = "rgb("+colors[i][1][0]+", "+colors[i][1][1]+", "+colors[i][1][2]+")";      
        document.getElementById('div'+(i+1)+'Color3').style.backgroundColor = "rgb("+colors[i][2][0]+", "+colors[i][2][1]+", "+colors[i][2][2]+")";    
        document.getElementById('div'+(i+1)+'Color4').style.backgroundColor = "rgb("+colors[i][3][0]+", "+colors[i][3][1]+", "+colors[i][3][2]+")";      
        document.getElementById('div'+(i+1)+'Color5').style.backgroundColor = "rgb("+colors[i][4][0]+", "+colors[i][4][1]+", "+colors[i][4][2]+")";    

        // Text-Color
        document.getElementById('div'+(i+1)+'Color1').style.color = textColor;    
        document.getElementById('div'+(i+1)+'Color2').style.color = textColor;    
        document.getElementById('div'+(i+1)+'Color3').style.color = textColor;    
        document.getElementById('div'+(i+1)+'Color4').style.color = textColor;    
        document.getElementById('div'+(i+1)+'Color5').style.color = textColor;    
    }
    
    // Generate Rules
    var cssRules = document.getElementById('cssRules');
    cssRules.innerHTML = generateRules(colors);
}

/****************  Events  ****************/

// Init matriz
var rows = 5;
var cols = 5;

// Init Palette and Rules
var baseColors = [];
for (let i=0; i<rows; i++) {
    var baseColor = [];
    for (let j=0; j<cols; j++) {
        var color = [255, 255, 255];
        baseColor.push(color);
    }
    baseColors.push(baseColor);
}
var baseColors = [baseColor, baseColor, baseColor, baseColor, baseColor];
newPaletteRules(baseColors, "black");

// Event New Palette and Rules
var newColors = document.getElementById('newColors');
newColors.addEventListener('click', function() {
    // Random Colors
    var colors = randomColors(rows, cols);

    // Generate Palette and Rules
    newPaletteRules(colors, "white");
});

// Event Palette and Rules 
var cleanColors = document.getElementById('cleanColors');
cleanColors.addEventListener('click', function() {
    // Generate Palette and Rules
    newPaletteRules(baseColors, "black");
});