$(function() {

    //var urlServer = "http://192.168.0.11:8080/api/runTestVRT";
    var urlServer = "https://floating-shore-32862.herokuapp.com";
    var currentDate = "";
    var reports = 0;

    function getDateInFormat_ddmmyyyyhhmmss() {
        now = new Date();
        year = "" + now.getFullYear();
        month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
        day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
        hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
        minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
        second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
        return day + "-" + month + "-" + year + " " + hour + ":" + minute + ":" + second;
    }

    function convertImgBase64ToFile(dataurl) {
        var arr = dataurl.split(',');
        var mime = arr[0].match(/:(.*?);/)[1];
        var bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while(n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], 'a.png', {type:mime});
    }

    function onComplete(data) {
        $("#fecha").html("Fecha del reporte: "+currentDate);

        var diffImage = new Image();
        diffImage.src = data.getImageDataUrl();
        diffImage.className = "img-fluid img-thumbnail img-diff";

        var link = document.createElement('a');
        link.href = diffImage.src;
        link.download = "report"+reports+"_diffImg_"+currentDate+".png";
        link.appendChild(diffImage);

        $("#img-diff").html(link);
        $("#label-info").hide();

        if (data.misMatchPercentage == 0) {
            $("#diff-results-equals").show();
            $("#diff-results").hide();
        } else {
            $("#mismatch").text(data.misMatchPercentage);
            if (!data.isSameDimensions) {
                $("#diff-results-different-dimensions").show();
            } else {
                $("#diff-results-different-dimensions").hide();
            }
            $("#diff-results").show();
            $("#diff-results-equals").hide();
        }
    }

    $("#new-report").click(function() {
        var divReports = document.getElementById("reports");
        var currentReport = document.getElementById("report-"+reports);
        var cloneReport = document.getElementById('report-0').cloneNode(true);

        reports = reports + 1;
        cloneReport.id = "report-"+reports;
        cloneReport.style.display = "block";

        divReports.insertBefore(cloneReport, currentReport);
        if (reports == 1) {
            currentReport.style.display = "none";
        }

        currentDate = getDateInFormat_ddmmyyyyhhmmss();

        $("#new-report").hide();
        $("#wait-report").show();

        var img1Base64 = [];
        var img2Base64 = [];
        $.ajax({
            type: 'get',
            url: urlServer+"/api/runTestVRT/"+reports+"-1"
        }).then(function(data) {
            img1Base64 = "data:image/png;base64, "+data.data;
            var nameDownloadImg = "report"+reports+"_img1_"+currentDate;
            $("#img-1").html('<a href="'+img1Base64+'" download="'+nameDownloadImg+'.png"><img src="'+img1Base64+'" class="img-fluid img-thumbnail img"/></a>');

            $.ajax({
                type: 'get',
                url: urlServer+"/api/runTestVRT/"+reports+"-2"
            }).then(function(data) {
                img2Base64 = "data:image/png;base64, "+data.data;
                var nameDownloadImg = "report"+reports+"_img2_"+currentDate;
                $("#img-2").html('<a href="'+img2Base64+'" download="'+nameDownloadImg+'.png"><img src="'+img2Base64+'" class="img-fluid img-thumbnail img"/></a>');

                var file1 = convertImgBase64ToFile(img1Base64);
                var file2 = convertImgBase64ToFile(img2Base64);

                resembleControl = resemble(file1)
                    .compareTo(file2)
                    .onComplete(onComplete);

                $("#wait-report").hide();
                $("#new-report").show();
            });
        });
    });
});
