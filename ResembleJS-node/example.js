var resemble = require('./resemble.js');
var fs = require('fs');

var fileData1 = fs.readFileSync('./images/test1_crearCuentaExitosa/imagen1.png');
var fileData2 = fs.readFileSync('./images/test1_crearCuentaExitosa/People2.png');
resemble(fileData1).compareTo(fileData2)
  .ignoreLess()
  .onComplete(function (data) {
    console.log('Ignore less 1:', data);
    data.getDiffImage().pack().pipe(fs.createWriteStream('./images/test1_crearCuentaExitosa/Comparacion.png'));
  });

var fileData1 = fs.readFileSync('./images/test2_crearCuentaFallida/imagen1.png');
var fileData2 = fs.readFileSync('./images/test2_crearCuentaFallida/People2.png');
resemble(fileData1).compareTo(fileData2)
  .ignoreLess()
  .onComplete(function (data) {
    console.log('Ignore less 2:', data);
    data.getDiffImage().pack().pipe(fs.createWriteStream('./images/test2_crearCuentaFallida/Comparacion.png'));
  });

var fileData1 = fs.readFileSync('./images/test3_buscarProfesor/imagen1.png');
var fileData2 = fs.readFileSync('./images/test3_buscarProfesor/People2.png');
resemble(fileData1).compareTo(fileData2)
  .ignoreLess()
  .onComplete(function (data) {
    console.log('Ignore less 3:', data);
    data.getDiffImage().pack().pipe(fs.createWriteStream('./images/test3_buscarProfesor/Comparacion.png'));
  });

var fileData1 = fs.readFileSync('./images/test4_buscarMateriaProfesor/imagen1.png');
var fileData2 = fs.readFileSync('./images/test4_buscarMateriaProfesor/People2.png');
resemble(fileData1).compareTo(fileData2)
  .ignoreLess()
  .onComplete(function (data) {
    console.log('Ignore less 4:', data);
    data.getDiffImage().pack().pipe(fs.createWriteStream('./images/test4_buscarMateriaProfesor/Comparacion.png'));
  });
