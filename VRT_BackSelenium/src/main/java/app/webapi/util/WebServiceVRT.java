package app.webapi.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebServiceVRT {
	
	private static String encodeFileToBase64Binary(File file) throws IOException {
		byte[] bytes = loadFile(file);
		byte[] encoded = Base64.encodeBase64(bytes);
		String encodedString = new String(encoded);

		return encodedString;
	}

	private static byte[] loadFile(File file) throws IOException {
		FileInputStream fileInputStream = null;
        byte[] bytesArray = null;
        try {
            bytesArray = new byte[(int) file.length()];
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bytesArray;
	}
	
	
	private static String takeScreenshot(WebDriver driver) {
		String imgBase64 = "";
    	try {
	    	File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	    	imgBase64 = encodeFileToBase64Binary(srcFile);
        } catch (IOException e) {
        	e.printStackTrace();
		}
    	return imgBase64;
 	}
	
	public static String runTest(String report, String img) {
		System.setProperty("webdriver.chrome.driver", "/app/.chromedriver/bin/chromedriver");
		
		// Open Driver
		ChromeOptions options = new ChromeOptions();
		options.addArguments("headless");
		options.addArguments("window-size=1024x768");
		options.addArguments("-incognito");
		WebDriver driver = new ChromeDriver(options);
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		// Test
		String urlPage = "https://jscastrom.gitlab.io/taller-6/ColorsPallete/";
		driver.navigate().to(urlPage);
		Actions actions = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		By element = By.xpath("//button[@id='newColors']");
		WebElement webElement = wait.until(ExpectedConditions.elementToBeClickable(element));
		actions.moveToElement(webElement).click().perform();
		By element2 = By.xpath("//h2[contains(.,'Paleta de colores')]");
		WebElement webElement2 = wait.until(ExpectedConditions.elementToBeClickable(element2));
		actions.moveToElement(webElement2).click().perform();
		
		// Take Screenshot
		String imgBase64 = takeScreenshot(driver);
		
		// Close Driver
		driver.quit();
		
		return imgBase64;
	}
	
}
