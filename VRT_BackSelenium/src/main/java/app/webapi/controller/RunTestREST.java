package app.webapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.webapi.model.ImgBase64;
import app.webapi.util.WebServiceVRT;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class RunTestREST {
	
	@GetMapping("/runTestVRT/{params}")
	public ImgBase64 getStatusApp(@PathVariable String params) {
		ImgBase64 img = new ImgBase64();
		String reportNum = params.split("-")[0];
		String imgNum = params.split("-")[1];
		String data = WebServiceVRT.runTest(reportNum, imgNum);
		img.setData(data);
		return img;
	}
}
